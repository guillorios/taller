<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Departamentos</title>
</head>
<body>
    <h1>Departamentos</h1>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Departamento</th>
            <th scope="col">Codigo</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($departments as $department)
                <tr>
                    <th scope="row">{{ $department['Id'] }}</th>
                    <td>{{ $department['Name'] }}</td>
                    <td>{{ $department['StateId'] }}</td>
                </tr>
            @endforeach
        </tbody>
      </table>
</body>
</html>
