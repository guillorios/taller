<label for="first_name">Empleado</label>
<select class="form-control select2" name="employee_id" id="employee_id" title="Empleado" data-size="7">
    <option disabled selected>Escoja el empleado...</option>
    @foreach ($employees as $employee)
        <option value="{{ $employee->id }}"
        >{{ $employee->first_name }} {{ $employee->last_name }}</option>
    @endforeach
</select>
<br>

<label for="last_name">Departamento</label>
<select class="form-control select2" name="department_id" id="department_id" title="Departamento" data-size="7">
    <option disabled selected>Escoja el departamento...</option>
    @foreach ($departments as $department)
        <option value="{{ $department['Id'] }}"
        >{{ $department['Name']}}</option>
    @endforeach
</select>
<br>

<br>

<input type="submit">
