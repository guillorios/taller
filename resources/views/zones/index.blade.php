<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zonas</title>
</head>
<body>
    <h1>Zonas</h1> <a href="{{ route('zones.create') }}">Crear Nueva Zona</a>
    <br>
    <table class="table table-light">
        <thead class="thead thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Empleado</th>
            <th scope="col">Zona</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($zones as $zone)
                <tr>
                    <th scope="row">{{ $zone->id }}</th>
                    <td>{{ $zone->getEmployee() }}</td>
                    <td>{{ $zone->department_id }}</td>
                </tr>
            @endforeach
        </tbody>
      </table>
</body>
</html>

