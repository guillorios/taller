<h1>Crear Zona</h1>

<form action="{{ route('zones.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    @include('zones.form')
</form>
