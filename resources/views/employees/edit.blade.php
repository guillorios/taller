<h1>Editar Empleado</h1>

<form action="{{ route('employee.update', $employee->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    {{ method_field('PATCH') }}
    @include('employees.form')
</form>
