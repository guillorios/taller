<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Empleados</title>
</head>
<body>
    <h1>Empleados</h1> <a href="{{ route('employee.create') }}">Crear Nuevo</a>
    <br>
    <table class="table table-light">
        <thead class="thead thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombres</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Cumpleaños</th>
            <th scope="col">Genero</th>
            <th scope="col">Fecha Contrato</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
                <tr>
                    <th scope="row">{{ $employee->id }}</th>
                    <td>{{ $employee->first_name }}</td>
                    <td>{{ $employee->last_name }}</td>
                    <td>{{ $employee->birthday }}</td>
                    <td>{{ $employee->gender }}</td>
                    <td>{{ $employee->hire }}</td>
                    <td>
                        <a href="{{ url('employee/'. $employee->id. '/edit') }}">Editar</a>
                        |
                        <form action="{{ url('employee/'. $employee->id) }}" method="post">
                            @csrf
                            {{ method_field('DELETE') }}
                            <input type="submit" onclick="return confirm('Desea borrar ?')" value ="Borrar">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
      </table>
</body>
</html>
