<label for="first_name">Nombres</label>
<input type="text" name="first_name" id="first_name" value="{{ isset($employee->first_name) ? $employee->first_name:''  }}">
<br>

<label for="last_name">Apellidos</label>
<input type="text" name="last_name" id="last_name" value="{{ isset($employee->last_name) ? $employee->last_name:'' }}">
<br>

<label for="birthday">Cumpleaños</label>
<input type="date" name="birthday" id="birthday" value="{{ isset($employee->birthday) ? $employee->birthday:'' }}">
<br>

<label for="gender">Genero</label>
<input type="text" name="gender" id="gender" value="{{ isset($employee->gender) ? $employee->gender:'' }}">
<br>

<label for="hire">Fecha Contrato </label>
<input type="date" name="hire" id="hire" value="{{ isset($employee->hire) ? $employee->hire:'' }}">
<br>

<input type="submit">
