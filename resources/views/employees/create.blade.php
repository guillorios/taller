<h1>Crear Empleado</h1>

<form action="{{ route('employee.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    @include('employees.form')
</form>
