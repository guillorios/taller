<?php

namespace App\Http\Controllers;

use App\Models\Zone;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $zones = Zone::get();

        return view('zones.index', [
            'zones' => $zones,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $departments = HTTP::get('https://intcomex-test.apigee.net/v1/getplaces?locale=es&countryId=Co&apiKey=5012b669-f1eb-4eb4-bda8-d69d1ada3086&utcTimeStamp=2020-05-07T21:00:11.071Z&signature=b43f6d5eb1f9fa98388f2b69b01dabc1c6046e8f7a87e3ca99efe2d4dae04ba0')->json();
        $employees = Employee::get();

        return view('zones.create', [
            'departments' => $departments,
            'employees' => $employees
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosZone = request()->except('_token');

        Zone::insert($datosZone);

        return redirect('zones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
