<?php

namespace App\Models;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Zone extends Model
{
    use HasFactory;

    public function getEmployee(){

        if (!is_null($this->employee_id)){
            $query = Employee::where('id', $this->employee_id)->first();
            $fullName = "{$query->first_name} {$query->last_name}";
            return  $fullName;
        }

    }

    public function employees(){
        return $this->hasMany(Employee::class);
    }
}
